# This project is a weather information services.
 
##Based on a given city(name or id) it will return the Weather conditions.

This project is built in java 8 with spring boot and it consumes the OpenWeatherMap API to retrieve the information.


## Testing
The project comes ready with an instance of Gradle wrapper. To run the tests for the project execute the following command from within the project's root directory:

```
./gradlew clean test integrationTest testAcceptanceLocal -i
```
* `test` - executes unit tests
* `integrationTest` - executes spring-boot tests
* `testAcceptanceLocal` - executes cucumber tests 

## Running

### Insert key
It is necessary to insert a valid key in the configuration properties.
Replace: INSERT_YOUR_KEY in application.properties on main/resources/ 

### Docker 
 It is possible to launch the application as a container, in this case you need to have docker installed in your server,
  and then you just need to execute the next commands in the project's root:
  
 1. docker build -t weather-app:0.0.1 .
 2. docker run -p 8080:8080 weather-app:0.0.1

### Terminal 
This is a Spring Boot application, so it is packed as a jar. 
To start the app, execute:
```
java -jar <pathToJar>.jar

```
- after run the command "./gradlew build", you can find the jar on "/build/libs/statistc-service-0.0.1.jar" from the project's root
###IDE
Run the main class(Application.java) from any IDE.

## Used design Patterns: ##
1. Builder
2. Strategy
3. Singleton

## Next steps:
1. Insert a cache layer to avoid duplicate request in the api
2. Create a fallback mechanism
3. Create health check, to test all the external dependencies.
4. It is necessary to insert authentication in the API.
5. Create a circuit breaker, to handle eventual dependency downtime.
