package com.mjs.weatherapp.server.dao.openWeatherMapApi;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mjs.weatherapp.server.dao.WeatherDao;
import com.mjs.weatherapp.server.exceptions.NotFoundException;
import com.mjs.weatherapp.server.model.TemperatureSummary;
import com.mjs.weatherapp.server.model.WeatherCondition;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class WeatherDaoOpenWeather implements WeatherDao {

    private static final Logger logger = LoggerFactory.getLogger(WeatherDaoOpenWeather.class);

    public static final String RATES_FIELD = "rates";

    @Autowired
    private OpenWeatherProvider openWeatherProvider;

    @Value("${openweather.api.id}")
    private String openExchangeApiKey;

    @Override
    public WeatherCondition getWeatherConditionFor(String cityName) throws IOException {

        logger.debug(String.format("Fetch weather data for city %s will be performed.", cityName));
        JsonObject resultAsJson = null;

        try {
            resultAsJson = openWeatherProvider.getWeatherForCity(cityName, openExchangeApiKey);
        } catch (FeignException ex) {
            if (ex.status() == 404) {
                throw new NotFoundException(String.format("%s not found", cityName));
            }

            logger.error(String.format("Client return the error '%s'", ex.getCause()));
            throw new IOException(ex.getCause());
        } catch (Exception ex) {
            logger.error(String.format("Unexpected error '%s'", ex.getCause()));
            throw ex;
        }

        return toWeatherConditionFrom(resultAsJson);
    }

    @Override
    public List<WeatherCondition> getWeatherConditionListFor(List<Integer> ids) throws IOException {
        Map<String, String> map = new HashMap<String, String>();

        String idsAsString = transformIdIntoCsv(ids);

        logger.debug(String.format("Fetch weather data for city ids %s will be performed.", ids));
        JsonObject resultAsJson = null;
        try {
            resultAsJson = openWeatherProvider.getWeatherBatch(idsAsString, openExchangeApiKey);
        } catch (FeignException ex) {
            logger.error(String.format("Client return the error '%s'", ex.getCause()));
            throw new IOException(ex.getCause());
        } catch (Exception ex) {
            logger.error(String.format("Unexpected error '%s'", ex.getCause()));
            throw ex;
        }

        return buildWeatherConditionListFrom(resultAsJson);
    }

    private String transformIdIntoCsv(List<Integer> ids) {
        String idAsString = "";

        for (Integer id : ids) {
            if (idAsString != "") {
                idAsString = idAsString + ",";
            }
            idAsString += id;
        }

        return idAsString;
    }

    private List<WeatherCondition> buildWeatherConditionListFrom(JsonObject responseAsJson) throws IOException {
        List<WeatherCondition> weatherConditions = new ArrayList<>();

        JsonArray weatherConditionsAsJson = responseAsJson.get("list").getAsJsonArray();

        for (JsonElement jsonElement : weatherConditionsAsJson) {
            weatherConditions.add(toWeatherConditionFrom(jsonElement.getAsJsonObject()));
        }

        return weatherConditions;
    }

    private WeatherCondition toWeatherConditionFrom(JsonObject responseAsJson) {
        String cityName = responseAsJson.get("name").getAsString();
        JsonObject mainNode = responseAsJson.getAsJsonObject("main");

        TemperatureSummary temperatureSummary = buildTemperatureSummaryFrom(mainNode);
        JsonObject jsonObjectWeather = responseAsJson.getAsJsonArray("weather").get(0).getAsJsonObject();

        return buildWeatherCondition(jsonObjectWeather, cityName, temperatureSummary);
    }

    private TemperatureSummary buildTemperatureSummaryFrom(JsonObject mainJsonObject) {
        return new TemperatureSummary(mainJsonObject.get("temp").getAsDouble(), mainJsonObject.get("pressure")
                .getAsInt(),
                mainJsonObject.get("humidity").getAsInt(), mainJsonObject.get("temp_max").getAsDouble(),
                mainJsonObject.get("temp_min").getAsDouble());
    }

    private WeatherCondition buildWeatherCondition(JsonObject mainJsonObject, String name,
                                                   TemperatureSummary temperatureSummary) {

        return new WeatherCondition(name, mainJsonObject.get("main").getAsString(),
                mainJsonObject.get("description").getAsString(), mainJsonObject.get("icon").getAsString(),
                temperatureSummary);

    }
}
