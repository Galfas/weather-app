package com.mjs.weatherapp.server.dao.openWeatherMapApi;

import com.google.gson.JsonObject;
import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface OpenWeatherProvider {

    @RequestLine("GET /data/2.5/weather?q={cityName}&appid={appId}")
    JsonObject getWeatherForCity(@Param("cityName") String cityName, @Param("appId") String appId);

    @RequestLine("GET /data/2.5/group?id={cityIds}&units=metric&appid={appId}")
    JsonObject getWeatherBatch(@Param("cityIds") String cityIds, @Param("appId") String appId);
}
