package com.mjs.weatherapp.server.controller;

import com.mjs.weatherapp.server.business.WeatherBo;
import com.mjs.weatherapp.server.model.WeatherCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/weather")
public class WeatherController extends BaseController {

    @Autowired
    WeatherBo weatherBo;

    @RequestMapping("/{city}")
    @CrossOrigin(origins = "*")
    public WeatherCondition getWeatherConditionyFor(@PathVariable String city) throws IOException {

        return weatherBo.getWeatherConditionFor(city);
    }

    @RequestMapping("/ids/{ids}")
    @CrossOrigin(origins = "*")
    public List<WeatherCondition> getWeatherConditionListFor(@PathVariable List<Integer> ids) throws IOException {
        return weatherBo.getWeatherConditionListFor(ids);
    }
}
