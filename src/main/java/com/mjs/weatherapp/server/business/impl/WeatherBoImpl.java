package com.mjs.weatherapp.server.business.impl;

import com.mjs.weatherapp.server.business.WeatherBo;
import com.mjs.weatherapp.server.dao.WeatherDao;
import com.mjs.weatherapp.server.model.WeatherCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class WeatherBoImpl implements WeatherBo {

    @Autowired
    private WeatherDao weatherDao;

    @Override
    public WeatherCondition getWeatherConditionFor(String city) throws IOException {

        return weatherDao.getWeatherConditionFor(city);
    }

    @Override
    public List<WeatherCondition> getWeatherConditionListFor(List<Integer> ids) throws IOException {

        return weatherDao.getWeatherConditionListFor(ids);
    }
}
