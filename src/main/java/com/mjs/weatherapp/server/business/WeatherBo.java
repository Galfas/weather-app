package com.mjs.weatherapp.server.business;

import com.mjs.weatherapp.server.model.WeatherCondition;

import java.io.IOException;
import java.util.List;

public interface WeatherBo {

    WeatherCondition getWeatherConditionFor(String city) throws IOException;

    List<WeatherCondition> getWeatherConditionListFor(List<Integer> ids) throws IOException;
}
