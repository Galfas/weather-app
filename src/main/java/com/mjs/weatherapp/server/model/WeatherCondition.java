package com.mjs.weatherapp.server.model;

public class WeatherCondition {

    private String cityName;

    private String main;

    private String description;

    private String icon;

    private TemperatureSummary temperatureSummary;

    public WeatherCondition() {
    }

    public WeatherCondition(String cityName, String main, String description, String icon, TemperatureSummary
            temperatureSummary) {
        this.cityName = cityName;
        this.main = main;
        this.description = description;
        this.icon = icon;
        this.temperatureSummary = temperatureSummary;
    }

    public String getCityName() {
        return cityName;
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public TemperatureSummary getTemperatureSummary() {
        return temperatureSummary;
    }
}


