package com.mjs.weatherapp.server.model;

public class TemperatureSummary {

    private Double currentTemperature;

    private Integer pressure;

    private Integer humidity;

    private Double maxTemperature;

    private Double minTemperature;

    public TemperatureSummary() {
    }

    public TemperatureSummary(Double currentTemperature, Integer pressure, Integer humidity, Double maxTemperature,
                              Double minTemperature) {
        this.currentTemperature = currentTemperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.maxTemperature = maxTemperature;
        this.minTemperature = minTemperature;
    }

    public Double getCurrentTemperature() {
        return currentTemperature;
    }

    public Integer getPressure() {
        return pressure;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public Double getMaxTemperature() {
        return maxTemperature;
    }

    public Double getMinTemperature() {
        return minTemperature;
    }
}
