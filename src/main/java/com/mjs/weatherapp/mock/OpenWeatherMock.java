package com.mjs.weatherapp.mock;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;

@Profile("mock")
@RestController
@RequestMapping(value = "/mock/weather")
public class OpenWeatherMock {

    public static final String MOCK_KEY = "d0a7d8aeaeca4f70a4bf377b4d15a72a";

    @GetMapping("/data/2.5/weather")
    ResponseEntity<String> fetchWeatherForCityName(@RequestParam String q,
                                                   @RequestParam String appid) throws IOException {
        return createResponseEntity(appid, String.format("%s-weather.json", q.toLowerCase()));
    }

    @GetMapping("/data/2.5/group")
    ResponseEntity<String> fetchWeatherForIds(@RequestParam List<Integer> id,
                                              @RequestParam String appid) throws IOException {
        return createResponseEntity(appid, "weather-batch.json");
    }

    private ResponseEntity createResponseEntity(String appId, String fileName) throws IOException {
        ResponseEntity responseEntity;
        System.out.println((fileName.toLowerCase().startsWith("invalid")));
        System.out.println((fileName.toLowerCase().startsWith("invalid")));
        if (!appId.equals(MOCK_KEY)) {
            responseEntity = new ResponseEntity(new HashMap<String, String>(), HttpStatus.BAD_REQUEST);
        } else if (fileName.toLowerCase().startsWith("invalid")) {
            responseEntity = new ResponseEntity(null, HttpStatus.NOT_FOUND);
        } else {
            String content = new String(Files.readAllBytes(getFile(fileName).toPath()));
            responseEntity = new ResponseEntity(content, HttpStatus.OK);
        }

        return responseEntity;
    }

    private File getFile(String fileName) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }
}
