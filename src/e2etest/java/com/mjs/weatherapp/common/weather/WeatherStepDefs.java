package com.mjs.weatherapp.common.weather;

import com.jayway.restassured.response.Response;
import com.mjs.weatherapp.common.BaseApiClient;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WeatherStepDefs {

    String cityName;

    String ids;

    Response response;

    RuntimeException exception;

    @Given("^I want to fetch weather information for \"([^\"]*)\"$")
    public void i_want_to_fetch_city_information(String cityName) {
        this.cityName = cityName;
    }

    @Given("^I want to fetch weather information for ids \"([^\"]*)\"$")
    public void when_i_request_the_application1(String ids) {
        this.ids = ids;
    }

    @Given("^I request the application$")
    public void when_i_request_the_application() {
        response = null;

        response = BaseApiClient.givenApiClient(BaseApiClient.DEFAULT_USER_TYPE).get(
                String.format("/api/weather/%s", cityName));
    }

    @Given("^I request the application for multiple cities$")
    public void when_i_request_the_application_multiple_ids() {
        response = null;

        response = BaseApiClient.givenApiClient(BaseApiClient.DEFAULT_USER_TYPE).get(
                String.format("/api/weather/ids/%s", ids));
    }

    @Then("^It Should had received an exception$")
    public void should_have_received_an_exception() {
        assertNotNull(exception);
        exception = null;
    }

    @Then("^I should have received a not Found response$")
    public void should_verify_not_found_response() {
        assertEquals(404, response.statusCode());
    }

    @Then("^I Should see \"([^\"]*)\" and \"([^\"]*)\" as maximum and minimum temperature$")
    public void should_verify_temperatures(String maximum, String minimum) {
        assertEquals(200, response.statusCode());

        Map<String, Object> temperatureSummary = (Map) response.body().as(Map.class).get("temperatureSummary");

        assertEquals(maximum, temperatureSummary.get("maxTemperature").toString());
        assertEquals(minimum, temperatureSummary.get("minTemperature").toString());
    }

    @And("^The main info should be \"([^\"]*)\" and the description \"([^\"]*)\"$")
    public void should_verify_main_and_description_info(String mainInfo, String
            description) {
        assertEquals(mainInfo, response.body().as(Map.class).get("main"));
        assertEquals(description, response.body().as(Map.class).get("description"));
    }

    //            I Should see "3" cities in the response
    @Then("^I should see \"([^\"]*)\" cities in the response$")
    public void check_quantity_of_cities(Integer quantity) {
        System.out.println(response);
        System.out.println(response);
        System.out.println(response);
        System.out.println(response);
        Integer size = response.body().as(List.class).size();
        assertEquals(quantity, size);
    }
}
