Feature: Retrieve weather information.

  Background:
    Given app has started
    And app is health

  Scenario: fetch weather conditions for a valid city
    Given I want to fetch weather information for "Berlin"
    When I request the application
    Then I Should see "400.0" and "200.0" as maximum and minimum temperature
    And The main info should be "Clouds" and the description "scattered clouds"

  Scenario: fetch weather conditions for a valid city
    Given I want to fetch weather information for "Dublin"
    When I request the application
    Then I Should see "700.0" and "300.0" as maximum and minimum temperature
    And The main info should be "Rain" and the description "rainy"

  Scenario: fetch weather conditions for a valid city
    Given I want to fetch weather information for ids "1,2,3"
    When I request the application for multiple cities
    Then I should see "3" cities in the response

  Scenario: Fetch weather conditions for invalid city
    Given I want to fetch weather information for "InvalidCity"
    When I request the application
    Then I should have received a not Found response
