package com.mjs.weatherapp.server.controller;

import com.mjs.weatherapp.server.business.WeatherBo;
import com.mjs.weatherapp.server.model.WeatherCondition;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.mjs.weatherapp.server.helpers.WeatherConditionBuilder.buidldWeatherCondition;
import static com.mjs.weatherapp.server.helpers.WeatherConditionBuilder.buidldWeatherConditionList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class WeatherControllerTest {

    @Mock
    WeatherBo weatherBo;

    @InjectMocks
    WeatherController weatherController = new WeatherController();

    @Test
    public void getWeatherConditionyFor_shouldFetchWeatherConditionExistentCity() throws IOException {
        String cityName = "Berlin";

        WeatherCondition defaultWeatherCondition = buidldWeatherCondition();

        Mockito.when(weatherBo.getWeatherConditionFor(cityName)).thenReturn(defaultWeatherCondition);

        WeatherCondition weatherCondition = weatherController.getWeatherConditionyFor(cityName);

        verify(weatherBo, times(1)).getWeatherConditionFor(cityName);

        Assert.assertNotNull(weatherCondition);
        Assert.assertSame(weatherCondition, defaultWeatherCondition);
    }

    @Test(expected = Exception.class)
    public void getWeatherConditionyFor_shouldReturnNotFoundForInexistentCity() throws IOException {
        String cityName = "invalid-city";

        Mockito.when(weatherBo.getWeatherConditionFor(cityName)).thenThrow(Exception.class);

        weatherController.getWeatherConditionyFor(cityName);
    }

    @Test
    public void getWeatherConditionyListFor_shouldFetchWeatherConditionExistentCity() throws IOException {
        List<Integer> cityIdList = Arrays.asList(1, 2);

        List<WeatherCondition> defaultWeatherConditionList = buidldWeatherConditionList();

        Mockito.when(weatherBo.getWeatherConditionListFor(cityIdList)).thenReturn(defaultWeatherConditionList);

        List<WeatherCondition> weatherConditionList = weatherController.getWeatherConditionListFor(cityIdList);

        verify(weatherBo, times(1)).getWeatherConditionListFor(cityIdList);

        Assert.assertNotNull(weatherConditionList);
        Assert.assertSame(weatherConditionList, defaultWeatherConditionList);
    }

    @Test(expected = Exception.class)
    public void getWeatherConditionyListFor_shouldReturnNotFoundForInexistentCity() throws IOException {
        List<Integer> cityIdList = Arrays.asList(-1, -2);

        Mockito.when(weatherBo.getWeatherConditionListFor(cityIdList)).thenThrow(Exception.class);

        weatherController.getWeatherConditionListFor(cityIdList);
    }
}
