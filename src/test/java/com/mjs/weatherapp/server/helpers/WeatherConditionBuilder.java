package com.mjs.weatherapp.server.helpers;

import com.mjs.weatherapp.server.model.TemperatureSummary;
import com.mjs.weatherapp.server.model.WeatherCondition;

import java.util.ArrayList;
import java.util.List;

public class WeatherConditionBuilder {

    public static List<WeatherCondition> buidldWeatherConditionList() {
        List<WeatherCondition> weatherConditions = new ArrayList<>();
        weatherConditions.add(buidldWeatherCondition());
        weatherConditions.add(buidldWeatherCondition());

        return weatherConditions;
    }

    public static WeatherCondition buidldWeatherCondition() {
        return new WeatherCondition("nameCity", "Clouds", "scattered clouds", "03n",
                new TemperatureSummary(300.15, 1007, 74, 300.15,
                        300.15));
    }
}
