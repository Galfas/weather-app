package com.mjs.weatherapp.server.dao;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mjs.weatherapp.server.dao.openWeatherMapApi.OpenWeatherProvider;
import com.mjs.weatherapp.server.dao.openWeatherMapApi.WeatherDaoOpenWeather;
import com.mjs.weatherapp.server.model.WeatherCondition;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;

@RunWith(SpringJUnit4ClassRunner.class)
public class WeatherDaoOpenWeatherTest {

    @Mock
    private OpenWeatherProvider openWeatherProvider;

    @InjectMocks
    WeatherDaoOpenWeather weatherDaoOpenWeather = new WeatherDaoOpenWeather();

    @Test
    public void getWeatherConditionFor_shouldFetchWeatherConditionForCity() throws IOException {
        String cityName = "Berlin";
        ClassLoader classLoader = this.getClass().getClassLoader();
        String jsonAsString = IOUtils.toString(classLoader.getResourceAsStream("weather.json"), "UTF-8");

        JsonObject jsonFile = new Gson().fromJson(jsonAsString, JsonObject.class);

        Mockito.when(openWeatherProvider.getWeatherForCity(anyString(), anyString()))
                .thenReturn(jsonFile);
        WeatherCondition weatherCondition = weatherDaoOpenWeather.getWeatherConditionFor(cityName);

        Assert.assertNotNull(weatherCondition);
        Assert.assertEquals(weatherCondition.getMain(), "Clouds");
        Assert.assertEquals(weatherCondition.getDescription(), "scattered clouds");
        Assert.assertEquals(weatherCondition.getTemperatureSummary().getMinTemperature(), Double.valueOf(300.00));
        Assert.assertEquals(weatherCondition.getTemperatureSummary().getMaxTemperature(), Double.valueOf(300.10));
    }

    @Test
    public void getWeatherConditionFor_shouldFetchWeatherConditionForCitiesById() throws IOException {
        List<Integer> cityIdList = Arrays.asList(1, 2);
        ClassLoader classLoader = this.getClass().getClassLoader();
        String jsonAsString = IOUtils.toString(classLoader.getResourceAsStream("weather-batch.json"), "UTF-8");

        JsonObject jsonFile = new Gson().fromJson(jsonAsString, JsonObject.class);

        Mockito.when(openWeatherProvider.getWeatherBatch(anyString(), anyString()))
                .thenReturn(jsonFile);
        List<WeatherCondition> weatherConditionList = weatherDaoOpenWeather.getWeatherConditionListFor(cityIdList);

        Assert.assertNotNull(weatherConditionList);
        Assert.assertEquals(3, weatherConditionList.size());

        Assert.assertEquals(weatherConditionList.get(0).getCityName(), "Moscow");
        Assert.assertEquals(weatherConditionList.get(0).getMain(), "Clear");
        Assert.assertEquals(weatherConditionList.get(0).getDescription(), "clear sky");
        Assert.assertEquals(weatherConditionList.get(0).getTemperatureSummary().getMinTemperature(),
                Double.valueOf(-11));
        Assert.assertEquals(weatherConditionList.get(0).getTemperatureSummary().getMaxTemperature(),
                Double.valueOf(-10));

        Assert.assertEquals(weatherConditionList.get(1).getCityName(), "Kiev");
        Assert.assertEquals(weatherConditionList.get(1).getMain(), "Clear");
        Assert.assertEquals(weatherConditionList.get(1).getDescription(), "clear sky");
        Assert.assertEquals(weatherConditionList.get(1).getTemperatureSummary().getMinTemperature(),
                Double.valueOf(-15));
        Assert.assertEquals(weatherConditionList.get(1).getTemperatureSummary().getMaxTemperature(),
                Double.valueOf(-9));

        Assert.assertEquals(weatherConditionList.get(2).getCityName(), "London");
        Assert.assertEquals(weatherConditionList.get(2).getMain(), "Mist");
        Assert.assertEquals(weatherConditionList.get(2).getDescription(), "mist");
        Assert.assertEquals(weatherConditionList.get(2).getTemperatureSummary().getMinTemperature(),
                Double.valueOf(5));
        Assert.assertEquals(weatherConditionList.get(2).getTemperatureSummary().getMaxTemperature(),
                Double.valueOf(8));

    }
}
