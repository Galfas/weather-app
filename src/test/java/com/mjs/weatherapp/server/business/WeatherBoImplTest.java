package com.mjs.weatherapp.server.business;

import com.mjs.weatherapp.server.exceptions.NotFoundException;
import com.mjs.weatherapp.server.business.impl.WeatherBoImpl;
import com.mjs.weatherapp.server.dao.WeatherDao;
import com.mjs.weatherapp.server.model.WeatherCondition;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.mjs.weatherapp.server.helpers.WeatherConditionBuilder.buidldWeatherCondition;
import static com.mjs.weatherapp.server.helpers.WeatherConditionBuilder.buidldWeatherConditionList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class WeatherBoImplTest {

    @Mock
    WeatherDao weatherDao;

    @InjectMocks
    WeatherBo weatherBo = new WeatherBoImpl();

    @Test
    public void getWeatherConditionFor_shouldFetchWeatherConditionExistentCity() throws IOException {
        String cityName = "Berlin";

        WeatherCondition defaultWeatherCondition = buidldWeatherCondition();

        Mockito.when(weatherDao.getWeatherConditionFor(cityName)).thenReturn(defaultWeatherCondition);

        WeatherCondition weatherCondition = weatherBo.getWeatherConditionFor(cityName);

        verify(weatherDao, times(1)).getWeatherConditionFor(cityName);

        Assert.assertNotNull(weatherCondition);
        Assert.assertSame(weatherCondition, defaultWeatherCondition);
    }

    @Test(expected = NotFoundException.class)
    public void getWeatherConditionyFor_getWeatherConditionFor() throws IOException {
        String cityName = "invalid-city";

        Mockito.when(weatherDao.getWeatherConditionFor(cityName)).thenThrow(NotFoundException.class);

        weatherBo.getWeatherConditionFor(cityName);
    }

    @Test
    public void getWeatherConditionListFor_shouldFetchWeatherConditionExistentCity() throws IOException {
        List<Integer> cityIdList = Arrays.asList(1, 2);

        List<WeatherCondition> defaultWeatherConditionList = buidldWeatherConditionList();

        Mockito.when(weatherDao.getWeatherConditionListFor(cityIdList)).thenReturn(defaultWeatherConditionList);

        List<WeatherCondition> weatherConditionList = weatherBo.getWeatherConditionListFor(cityIdList);

        verify(weatherDao, times(1)).getWeatherConditionListFor(cityIdList);

        Assert.assertNotNull(weatherConditionList);
        Assert.assertSame(weatherConditionList, defaultWeatherConditionList);
    }

    @Test(expected = NotFoundException.class)
    public void getWeatherConditionListFor_shouldReturnNotFoundForInexistentCity() throws IOException {
        List<Integer> cityIdList = Arrays.asList(-1, -2);
        WeatherCondition defaultWeatherCondition = buidldWeatherCondition();

        Mockito.when(weatherDao.getWeatherConditionListFor(cityIdList)).thenThrow(NotFoundException.class);

        weatherBo.getWeatherConditionListFor(cityIdList);
    }

}
