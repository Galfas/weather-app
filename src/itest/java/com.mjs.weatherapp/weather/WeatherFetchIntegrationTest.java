package com.mjs.weatherapp.weather;

import com.mjs.weatherapp.AbstractIntegrationTest;
import com.mjs.weatherapp.server.model.WeatherCondition;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class WeatherFetchIntegrationTest extends AbstractIntegrationTest {

    @Test
    public void shouldFetchWeatherConditionForGivenCity() throws Exception {
        String cityName = "Berlin";

        MvcResult mvcResult = mockMvc.perform(get(
                String.format("/api/weather/%s", cityName))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        WeatherCondition weatherCondition = mapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), WeatherCondition.class);

        assertEquals("Clouds", weatherCondition.getMain());
        assertEquals("scattered clouds", weatherCondition.getDescription());
        assertEquals("03n", weatherCondition.getIcon());
        assertEquals(Integer.valueOf(74), weatherCondition.getTemperatureSummary().getHumidity());
        assertEquals(Integer.valueOf(1007), weatherCondition.getTemperatureSummary().getPressure());
    }
}
